package ch.entities.javainstaller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.ini4j.Wini;

/**
 * <pre>
 * Copyright 2012 Pascal Vogt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * </pre>
 */
public class ConfigReader {
	private Wini ini;
	private String projectName;
	private String projectVersion;
	private String installerPrefix;
	private File warDirectory;
	private File projectDirectory;
	private File buildDirectory;
	private Environment[] environments;
	private String[] configurationFileIds;
	private String[] configurationFileTargetsPaths;
	private File[] configurationFileTargetFiles;
	private File outfile;
	
	public static class Environment {
		private String id;
		private String name;
		private String[] configurationFilePath;
		public String getId() {
			return id;
		}
		public String getName() {
			return name;
		}
		public String getConfigurationFilePath(int index) {
			return configurationFilePath[index];
		}
	}
	
	public ConfigReader(File file) throws IOException {
		this(new FileInputStream(file), file.getParentFile());
	}
	
	public void save(File file) throws IOException {
		ini.store(file);
	}
	
	public ConfigReader(InputStream fileInputStream) throws IOException {
		this(fileInputStream, null);
	}
	
	public ConfigReader(InputStream fileInputStream, File workingDirectory) throws IOException {
		ini = new Wini(fileInputStream) {
			private static final long serialVersionUID = 1L;

			@Override
			public String get(Object sectionName, Object optionName) {
				if (sectionName.equals("project")) {
					String value = System.getProperty(optionName.toString());
					if (value != null) {
						get("project").put(optionName.toString(), value);
						return value;
					}
				}
				return super.get(sectionName, optionName);
			}
		};
		projectName = ini.get("project", "name");
		projectVersion = ini.get("project", "version");
		outfile = new File(ini.get("project", "outfile"));
		installerPrefix = ini.get("project", "installer.prefix");
		warDirectory = new File(workingDirectory, ini.get("project", "war.dir"));
		projectDirectory = new File(workingDirectory, ini.get("project", "proj.dir"));
		buildDirectory = new File(workingDirectory, ini.get("project", "build.dir"));
		String[] environmentIds = trimAll(ini.get("project", "environments").split(","));
		configurationFileIds = trimAll(ini.get("project", "config").split(","));
		if (configurationFileIds.length == 1 && configurationFileIds[0].length() == 0) {
			configurationFileIds = new String[0];
		}
		configurationFileTargetsPaths = new String[configurationFileIds.length];
		configurationFileTargetFiles = new File[configurationFileIds.length];
		for (int i = 0; i < configurationFileIds.length; i++) {
			configurationFileTargetsPaths[i] = ini.get("project", "files." + configurationFileIds[i]);
			configurationFileTargetFiles[i] = new File(configurationFileTargetsPaths[i]);
		}
		environments = new Environment[environmentIds.length];
		for (int i = 0; i < environmentIds.length; i++) {
			environments[i] = new Environment();
			environments[i].id = environmentIds[i];
			environments[i].name = ini.get(environments[i].id, "name");
			environments[i].configurationFilePath = new String[configurationFileIds.length];
			for (int j = 0; j < configurationFileIds.length; j++) {
				environments[i].configurationFilePath[j] = ini.get(environments[i].id, "files." + configurationFileIds[j]);
			}
		}
	}
	
	public String getInstallerName() {
		return installerPrefix + "-" + projectVersion + ".jar";
	}

	public File getOutfile() {
		return outfile;
	}

	private static String[] trimAll(String[] array) {
		for (int i = 0; i < array.length; i++) {
			array[i] = array[i].trim();
		}
		return array;
	}

	public String getProjectName() {
		return projectName;
	}

	public String getProjectVersion() {
		return projectVersion;
	}

	public File getWarDirectory() {
		return warDirectory;
	}

	public File getBuildDirectory() {
		return buildDirectory;
	}

	public Environment[] getEnvironments() {
		return environments;
	}
	
	public Environment getEnvironmentById(String id) {
		for (int idx = 0; idx < environments.length; idx++) {
			if (id.equals(environments[idx].getId())) {
				return environments[idx];
			}
		}
		return null;
	}

	public String[] getConfigurationFileIds() {
		return configurationFileIds;
	}

	public File[] getConfigurationFileTargetFiles() {
		return configurationFileTargetFiles;
	}

	public File getProjectDirectory() {
		return projectDirectory;
	}
}
