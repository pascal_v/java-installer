package ch.entities.javainstaller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import ch.entities.javainstaller.ConfigReader.Environment;

/**
 * <pre>
 * Copyright 2012 Pascal Vogt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * </pre>
 */
public class Packager {
	private static void addToZip(ZipOutputStream zip, File addMe, File outPath) throws IOException {
		addToZip(zip, addMe, outPath, new File[]{});
	}

	private static void addToZip(ZipOutputStream zip, File addMe, File outPath, File[] excludes) throws IOException {
		String outPathStr = outPath.getPath();
		for (File exclude : excludes) {
			String excludePathStr = exclude.getPath();
			if (outPathStr.startsWith(excludePathStr)) {
				if (outPathStr.length() > excludePathStr.length()) {
					char separator = outPathStr.charAt(excludePathStr.length());
					if (separator == '/' || separator == '\\') {
						return;
					}
				} else {
					return;
				}
			}
		}
		
		if (addMe.isDirectory()) {
			for (File child : addMe.listFiles()) {
				addToZip(zip, child, new File(outPath, child.getName()), excludes);
			}
		} else if (addMe.isFile()) {
			zip.putNextEntry(new ZipEntry(outPathStr.replace('\\', '/')));
			
			FileInputStream fis = new FileInputStream(addMe.getCanonicalFile());
			
			byte[] bs = new byte[1024];
			int count;
	
			while ((count = fis.read(bs)) > 0) {
				zip.write(bs, 0, count);
			}
	
			fis.close();
		} else {
			throw new IllegalStateException("Don't know what to do with " + addMe);
		}
	}

	private static void addAllToZip(ZipOutputStream zip, File addMe, File outPath, File[] excludes) throws IOException {
		if (addMe.isFile()) {
			addToZip(zip, addMe, outPath, excludes);
		} else {
			for (File child : addMe.listFiles()) {
				addAllToZip(zip, child, new File(outPath, child.getName()), excludes);
			}
		}
	}

	private static void copyZipEntries(ZipOutputStream zipOut, ZipInputStream zipIn) throws IOException {
		ZipEntry entry;
		while ((entry = zipIn.getNextEntry()) != null) {
			zipOut.putNextEntry(new ZipEntry(entry.getName()));

			int count;
			byte[] bs = new byte[1024];

			while ((count = zipIn.read(bs, 0, bs.length)) > -1) {
				zipOut.write(bs, 0, count);
			}
		}
	}

	public static void main(String... args) throws IOException {
		ZipOutputStream installerZipOut = null;
		File installerFileOut = null;
		String[] configFiles = args;
		if (args.length == 0) {
			configFiles = new String[]{ "packager.ini" };
		}
		for (int configFileIdx = 0; configFileIdx < configFiles.length; configFileIdx++) {
			ConfigReader ini = new ConfigReader(new File(configFiles[configFileIdx]));
			
			/**
			 * Create the build directory if needed
			 */
			ini.getBuildDirectory().mkdirs();
			
			/**
			 * Start packaging installer
			 */
			if (configFileIdx == 0) {
				System.out.println("Packaging project " + ini.getProjectName() + ", version " + ini.getProjectVersion());
				installerFileOut = new File(ini.getBuildDirectory(), ini.getInstallerName());
				installerZipOut = new ZipOutputStream(new FileOutputStream(installerFileOut));
				installerZipOut.setLevel(ZipOutputStream.STORED);
				ZipInputStream installerZipIn = new ZipInputStream(ClassLoader.getSystemResourceAsStream("installer.jar"));
				copyZipEntries(installerZipOut, installerZipIn);
				installerZipIn.close();
			}
			
			/**
			 * Write the .ini file out again (some overrides might have taken place so can not copy it as-is 
			 */
			File configIniWithOverrides = new File(ini.getBuildDirectory(), "config.ini");
			ini.save(configIniWithOverrides);
			addToZip(installerZipOut, configIniWithOverrides, new File(configFileIdx + "-config.ini"));
			configIniWithOverrides.delete();
			
			/**
			 * Package environment-agnostic data and add it to the installer
			 */
			File contentsZipFile = new File(ini.getBuildDirectory(), "contents.zip");
			ZipOutputStream contentsZip = new ZipOutputStream(new FileOutputStream(contentsZipFile));
			File[] configurationFileTargetFiles = ini.getConfigurationFileTargetFiles();
			addAllToZip(contentsZip, ini.getWarDirectory(), null, configurationFileTargetFiles);
			contentsZip.close();
			addToZip(installerZipOut, contentsZipFile, new File(configFileIdx + "-contents.zip"));
			contentsZipFile.delete();
	
			/**
			 * Package environment-aware data for each environment and add it to the installer
			 */
			String[] configurationFileIds = ini.getConfigurationFileIds();
			int configurationFilesCount = configurationFileIds.length;
			if (configurationFilesCount > 0) {
				for (Environment environment : ini.getEnvironments()) {
					File configZipFile = new File(ini.getBuildDirectory(), "config-" + environment.getId() + ".zip");
					ZipOutputStream configZipOutputStream = new ZipOutputStream(new FileOutputStream(configZipFile));
					for (int j = 0; j < configurationFilesCount; j++) {
						File targetFile = new File(ini.getProjectDirectory(), environment.getConfigurationFilePath(j));
						addToZip(configZipOutputStream, targetFile, configurationFileTargetFiles[j]);
					}
					configZipOutputStream.close();
					addToZip(installerZipOut, configZipFile, new File(configFileIdx + "-config-" + environment.getId() + ".zip"));
					configZipFile.delete();
				}
			}
			
			/**
			 * Finalize installer
			 */
			if (configFileIdx == configFiles.length - 1) {
				installerZipOut.close();
				System.out.println("Done. Your installer is ready and located @ " + installerFileOut.getCanonicalPath());
			}
		}
	}
}
