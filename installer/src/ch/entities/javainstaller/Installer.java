package ch.entities.javainstaller;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import ch.entities.javainstaller.ConfigReader.Environment;

/**
 * <pre>
 * Copyright 2012 Pascal Vogt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * </pre>
 */
public class Installer {
	private static void copyZipEntries(ZipOutputStream zipOut, ZipInputStream zipIn) throws IOException {
		ZipEntry entry;
		while ((entry = zipIn.getNextEntry()) != null) {
			zipOut.putNextEntry(entry);

			int count;
			byte[] bs = new byte[1024];

			while ((count = zipIn.read(bs, 0, bs.length)) > -1) {
				zipOut.write(bs, 0, count);
			}
		}
	}

	public static void main(String... args) throws IOException {
		int configFileIdx = 0;
		while (true) {
			InputStream configFileStream = ClassLoader.getSystemResourceAsStream(configFileIdx + "-config.ini");
			if (configFileStream == null) {
				break;
			}
			ConfigReader ini = new ConfigReader(configFileStream);
	
			System.out.println("Installer for " + ini.getProjectName() + ", version " + ini.getProjectVersion());
			if (args.length != 1) {
				System.err.println("Pass one of the following environments to this program and it will create an environment specific file: ");
				Environment[] environments = ini.getEnvironments();
				for (Environment environment : environments) {
					System.out.println("- " + environment.getId() + " for " + environment.getName());
				}
				break;
			} else {
				String environmentId = args[0];
				Environment environment = ini.getEnvironmentById(environmentId);
				if (environment == null) {
					System.out.println("Unknown environment " + environmentId);
					System.out.flush();
				} else {
					System.out.println("Packaging deployable for " + environment.getName() + " ...");
					
					/**
					 * Open output file
					 */
					ZipOutputStream zipOut = new ZipOutputStream(new FileOutputStream(ini.getOutfile()));
					
					/**
					 * Copying environment-agnostic contents to output
					 */
					ZipInputStream contentsZip = new ZipInputStream(ClassLoader.getSystemResourceAsStream(configFileIdx + "-contents.zip"));
					copyZipEntries(zipOut, contentsZip);
					contentsZip.close();
					
					/**
					 * Copying environment-aware contents to output
					 */
					InputStream configZipInputStream = ClassLoader.getSystemResourceAsStream(configFileIdx + "-config-" + environment.getId() + ".zip");
					if (configZipInputStream != null) {
						ZipInputStream configZip = new ZipInputStream(configZipInputStream);
						copyZipEntries(zipOut, configZip);
						configZip.close();
					}
					
					/**
					 * Finalize
					 */
					zipOut.close();
					
					System.out.println("Done. Deployable is ready and located @ " + ini.getOutfile().getCanonicalPath());
				}
			}
			configFileIdx++;
		}
	}
}
