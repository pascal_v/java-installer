#!/bin/sh
if [ ! -f "ini4j-0.5.2.jar" ]; then
	wget http://repo1.maven.org/maven2/org/ini4j/ini4j/0.5.2/ini4j-0.5.2.jar
else
	echo "No download necessary"
fi
